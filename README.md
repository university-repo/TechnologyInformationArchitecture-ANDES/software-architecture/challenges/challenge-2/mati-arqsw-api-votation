## How to start

1. Create a virtual env and install all requirements.txt libraries
2. copy and paste the .env.example to a .env file with the values you'll need
3. run docker build -t appbrdm . and then 2. docker run -p 5001:80 appbrdm  or just run python app.py
4. enjoy.