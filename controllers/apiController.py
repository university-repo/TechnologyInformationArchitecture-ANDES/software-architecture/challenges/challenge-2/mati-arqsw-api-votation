import requests
class ApiController:
    def __init__(self, payload) -> None:
        self.components_endpoints = [
            "http://127.0.0.1:5000/v1/product/tax",
            "http://127.0.0.1:9080/v1/product/tax",
            "http://127.0.0.1:9081/v1/product/tax",
        ]
        self.payload = payload
        pass

    def getTax(self):
        """
        Calculates the tax by sending requests to multiple tax components and returns the most voted tax value.

        Returns:
            int: The most voted tax value.

        """
        tax_votes = []
        for i in range(len(self.components_endpoints)):
            tax_votes.append(self.sendRequestToTaxComponent(self.components_endpoints[i]))

        return self.vote(tax_votes)

        

    def sendRequestToTaxComponent(self, component):
        """
        Sends a request to a tax component and retrieves the tax value.

        Args:
            component (str): The URL of the tax component to send the request to.

        Returns:
            int: The tax value retrieved from the tax component.

        Raises:
            None

        """
        headers = {
            'X-RqUID': '12345',
            'X-Name': 'API node js challenge 2',
            'Content-Type': 'application/json',
        }
        result = {
            "status_code": 200,
            "IVA": 0,
        }
        response = requests.post(component, json=self.payload, headers=headers)

        if response.status_code == 200:
            # Obtenemos y imprimimos el JSON de la respuesta
            data = response.json()
            if self.verify_property(data, "IVA"):
                return float(data["IVA"])
            else:
                print("Component " + component + " is not sending the IVA correctly")
                return 0
        else:
            print("Component " + component + " is down for some reason")
            return 0
        
    
    def vote (self, votes):
        """
        Calculates the most voted value from a list of votes.

        Args:
            votes (list): A list of votes.

        Returns:
            The most voted value from the list.

        """
        votes_count = {}
        for vote in votes:
            if vote in votes_count:
                votes_count[vote] += 1
            else:
                votes_count[vote] = 1
        
        print(votes_count, max(votes_count, key=votes_count.get))
        return max(votes_count, key=votes_count.get)



    def verify_property(self, json_data, property_name):
        """
        Verifies if the parsed JSON data has the specified property.
        Args:
            json_data (dict): Parsed JSON data as dictionary.
            property_name (str): Name of the property to verify.
        Returns:
            bool: True if the property exists, False otherwise.
        """
        return property_name in json_data