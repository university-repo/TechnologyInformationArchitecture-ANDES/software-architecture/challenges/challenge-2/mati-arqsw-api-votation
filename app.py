import flask
from flask import request, jsonify, make_response
from flask_jwt_extended import JWTManager, jwt_required, create_access_token
import os
from dotenv import load_dotenv


from controllers import ApiController

load_dotenv()

app = flask.Flask(__name__)
jwt = JWTManager(app)
app.config["DEBUG"] = True

app.config["JWT_SECRET_KEY"] = os.getenv('JWT_SECRET_KEY')

"""
This function generates a success response with the provided data and status code.

Parameters:
- data: The data to be included in the response.
- status: The status code of the response. Default is 200.

Returns:
- A Flask response object with a JSON body containing the success status and the provided data.

Example Usage:
success_response({'name': 'John Doe', 'age': 30}, 200)
"""
def success_response(data, status=200):
    return make_response(jsonify({
        'success': True,
        'data': data
    }), status)

"""
This function generates an error response with the provided error message and status code.

Parameters:
- message: The error message to be included in the response.
- status: The status code of the response. Default is 400.

Returns:
- A Flask response object with a JSON body containing the error status and the provided error message.

Example Usage:
error_response('Invalid input', 400)
"""
def error_response(message, status=400):
    return make_response(jsonify({
        'success': False,
        'error': message
    }), status)


@app.route("/api/login", methods=["POST"])
def login():
    data = request.json
    if data is None:
        return error_response("False", 401)
    if 'user' in data and 'pass' in data:
        username = data['user']
        password = data['pass']
        if username == os.getenv('API_USER') and password == os.getenv('API_PASSWORD'):
            token = create_access_token(identity=username)
            return success_response({
                'token': token
            })
        else:
            return error_response("Error credentials", 401)
    else: 
        return error_response("Credentials missing", 401)
    

@app.route('/api/get-tax', methods=['GET'])
@jwt_required()
def getTax():
    data = request.json
    if data is None:
        return error_response("Missing parameters", 401)
    
    if 'id' in data and 'amount' in data and 'idClient' in data and  'email' in data:
        controler = ApiController({
            "id": data['id'],
            "amount": data['amount'],
            "idClient": data['idClient'],
            "email": data['email']
        })
        result = controler.getTax()
        return success_response({
            'IVA': result
        })
    else: 
        return error_response("Credentials parameters", 401)
    

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=80)